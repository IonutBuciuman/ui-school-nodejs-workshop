'use strict';

var http = require('http');

var postData = JSON.stringify({
	'msg' : 'Hello World!'
});

var options = {
	host: 'localhost',
	port: '8000',
	method: 'POST',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Content-Length': Buffer.byteLength(postData)
	}
};

console.info('options: ', options.headers);

var request = http.request(options, function (response) {
	console.info(response.statusCode);
	console.info(response.headers);
	
	response.on('data', function (chunk) {
		console.info('data..', chunk.toString());
	});
	
	response.on('end', function () {
		console.log('No more data in response.');
	});
});

request.on('error', function (error) {
    console.error(error);
});

request.write(postData);
request.end();