'use strict';

var fs = require('fs');
var http = require('http');
var server = http.createServer();

server.on('request', function (request, response) {
    response.writeHead(200, {
        'content-type': 'text/plain'
    });
    response.end(fs.readFileSync('./server-example-html/index.html'));
});

server.listen(8000);
