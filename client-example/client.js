'use strict';

var http = require('http');
var options = {
	method: 'GET',
	host: 'localhost',
	port: '8000'
};

var request = http.request(options, function (response) {
	console.info(response.statusCode);
	console.info(response.headers);
	var myData = '';
	
	response.on('data', function (chunk) {
		myData += chunk;
		console.info('data__\n', chunk.toString());
	});
	
	response.on('end', function () {
		console.info('raspuns primit___\n', myData);
	});
	
});

request.on('error', function (error) {
    console.error(error);
});

request.end();