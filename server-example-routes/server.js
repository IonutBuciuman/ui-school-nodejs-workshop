'use strict';

var fs = require('fs');
var http = require('http');
var server = http.createServer();

server.on('request', function (request, response) {
    switch (request.url) {
        case '/home':
            response.writeHead(200, { 'content-type': 'text/html' });
            response.end(fs.readFileSync('./server-example-routes/home.html').toString());
            break;

        case '/another':
            response.writeHead(200, { 'content-type': 'text/html' });
            response.end(fs.readFileSync('./server-example-routes/another-page.html'));
            break;

        case '/':
            console.info('here');
            response.writeHead(301, { 'location': '/error'});
            response.end();
            break;

        default:
            response.writeHead(200, { 'content-type': 'application/json' });
            response.end(JSON.stringify({
                nothing: 'to',
                show: '..'
            }));
            break;
    }

});

server.listen(8000);
console.info('Server starting on: localhost:8000');