'use strict';

const url = require('url');
const querystring = require('querystring');

console.info(url.parse('https://www.youtube.com/watch?v=E7kRQAy9tho&index=27&list=RDU3A-lJQlrmk'));
console.info('\n\n\n\n');
console.info(url.parse('http://www.imdb.com/title/tt3315342/?ref_=nv_sr_1'));

querystring.parse('http://www.imdb.com/title/tt3315342/?ref_=nv_sr_1');
querystring.parse('https://www.youtube.com/watch?v=E7kRQAy9tho&index=27&list=RDU3A-lJQlrmk');