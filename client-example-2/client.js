'use strict';

var http = require('http');

var request = http.request({
        host: 'localhost', port: '8000'
    }, function (response) {
        console.info(response.statusCode);
        console.info(response.headers);

        response.on('data', function (data) {
            console.info('data..', data.toString());
        });
    });

request.on('error', function (error) {
    console.error(error);
});

request.end();